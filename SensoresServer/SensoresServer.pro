#-------------------------------------------------
#
# Project created by QtCreator 2019-07-23T07:31:42
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SensoresServer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        dao/idatosensordao.cpp \
        dao/isensordao.cpp \
        dao/sqlitedatosensordao.cpp \
        dao/sqlitesensordao.cpp \
        model/datosensor.cpp \
        model/mediciondesensor.cpp \
        model/sensor.cpp \
        view/sensoreslistmodel.cpp \
        view/dialog.cpp \
        view/visualizador.cpp \
        view/sensoresdatamodel.cpp \
        service/isensoresservice.cpp \
        service/sensoresserviceimpl.cpp \
        net/messageparser.cpp \
        net/udpserver.cpp \
        db/dbutils.cpp \
        main.cpp

HEADERS += \
        dao/idatosensordao.h \
        dao/isensordao.h \
        dao/sqlitedatosensordao.h \
        dao/sqlitesensordao.h \
        model/datosensor.h \
        model/mediciondesensor.h \
        model/sensor.h \
        view/sensoresdatamodel.h \
        view/sensoreslistmodel.h \
        view/dialog.h \
        view/visualizador.h \
        service/isensoresservice.h \
        service/sensoresserviceimpl.h \
        net/messagereceiver.h \
        net/networkDefinitions.h \
        net/messageparser.h \
        net/udpserver.h \
        db/dbutils.h

FORMS += \
        view/dialog.ui \
        view/visualizador.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    readme.txt

RESOURCES += \
    resources.qrc \
    qdarkstyle/style.qrc

VERSION = 1.0.1.0
win32:RC_ICONS = icons/appsensoresserver.ico
QMAKE_TARGET_COMPANY = Company Martin
QMAKE_TARGET_DESCRIPTION = Aplicacion de captura y muestra de datos obtenidos de sensores
QMAKE_TARGET_COPYRIGHT = Este producto tiene Copyright de pirulo. Use at your own risk.
QMAKE_TARGET_PRODUCT = Sensores Server
