#ifndef ISENSORESSERVICE_H
#define ISENSORESSERVICE_H

#include <QList>
#include <QString>
#include "model/sensor.h"
#include "model/mediciondesensor.h"
#include "model/datosensor.h"

class ISensoresService
{
public:
    ISensoresService();
    virtual ~ISensoresService();

    virtual QList<Sensor*> * getAllSensors() = 0; // Obtiene todos los sensores
    virtual Sensor* getSensor(QString sensorId) = 0; // Obtiene el sensor de una lista de sensores
    virtual bool insertMedicion(MedicionDeSensor * data) = 0; // Agrega una medicion a un sensor
//    virtual QList<SensorMovData*>   getHistoryData(SensorMov * sensor) = 0; // obtiene el historial de datos de un sensor, podria recibir fevchas por parametro para filtrar
//    virtual QList<SensorRuidoData*> getHistoryData(SensorRuido * sensor) = 0; // obtiene el historial de datos de un sensor, podria recibir fevchas por parametro para filtrar
//    virtual QList<SensorTempData*>  getHistoryData(SensorTemp * sensor) = 0; // obtiene el historial de datos de un sensor, podria recibir fevchas por parametro para filtrar

    virtual QList<DatoSensor *> * getAllSensorData(Sensor * sensor) = 0;
};

#endif // ISENSORESSERVICE_H
