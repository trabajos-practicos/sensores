#include "sensoresserviceimpl.h"
#include <QDebug>

SensoresServiceImpl::SensoresServiceImpl(ISensorDao * sensorDao, IDatoSensorDao * datoSensorDao, QObject * parent /* = nullptr */) : QObject(parent)
{
    this->sensorDao = sensorDao;
    this->datoSensorDao = datoSensorDao;
    this->sensores = sensorDao->getAll();
}

SensoresServiceImpl::~SensoresServiceImpl()
{

}

bool SensoresServiceImpl::insertMedicion(MedicionDeSensor * data)
{
    QString tipo   = data->getTipo();
    QString nombre = data->getNombre();

    Sensor * sensor = getSensor(nombre);
    if(!sensor) {
        sensor = new Sensor(-1, nombre, tipo);
        if(sensorDao->insert(sensor)) {
            this->sensores->append(sensor);
        }
    }
    if(sensor) {
        if(tipo == "T") {
            datoSensorDao->insert(sensor, data->getValor().toFloat());
        } else if(tipo == "R") {
            datoSensorDao->insert(sensor, data->getValor().toInt());
        } else if(tipo == "M") {
            datoSensorDao->insert(sensor, data->getValor().toInt() == 0 ? false : true);
        }
    }
    return false;
}

Sensor *SensoresServiceImpl::getSensor(QString nombre)
{
    for (int i = 0; i < this->sensores->size(); i++) {
        Sensor * sensor = this->sensores->at(i);
        if (sensor->getNombre() == nombre)
            return sensor;
    }

    return nullptr;
}

QList<Sensor *> *  SensoresServiceImpl::getAllSensors()
{
    return this->sensores;
}

QList<DatoSensor *> * SensoresServiceImpl::getAllSensorData(Sensor * sensor)
{
    return datoSensorDao->getAll(sensor);
}
