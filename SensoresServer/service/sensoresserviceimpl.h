#ifndef SENSORESSERVICEIMPL_H
#define SENSORESSERVICEIMPL_H

#include <QObject>
#include "service/isensoresservice.h"
#include "dao/isensordao.h"
#include "dao/idatosensordao.h"

class SensoresServiceImpl : public QObject, public ISensoresService
{
public:
    SensoresServiceImpl(ISensorDao * sensorDao,IDatoSensorDao * datoSensorDao,  QObject * parent = nullptr);
    virtual ~SensoresServiceImpl() override;

    QList<Sensor*> * getAllSensors() override; // Obtiene todos los sensores
    Sensor* getSensor(QString sensorId) override; // Obtiene el sensor de una lista de sensores

    bool insertMedicion(MedicionDeSensor * data) override; // Agrega una medicion a un sensor

    QList<DatoSensor *> * getAllSensorData(Sensor * sensor) override;

private:
    ISensorDao * sensorDao;
    IDatoSensorDao * datoSensorDao;
    QList<Sensor*> * sensores;
};

#endif // SENSORESSERVICEIMPL_H
