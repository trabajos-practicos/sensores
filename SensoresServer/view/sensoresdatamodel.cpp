#include "sensoresdatamodel.h"
#include <QColor>
#include <QDebug>

SensoresDataModel::SensoresDataModel(QList<DatoSensor *> *datos, QObject *parent /* = nullptr */)
    : QAbstractTableModel(parent)
{
    this->datos = datos;
}

SensoresDataModel::~SensoresDataModel()
{
    if(datos)
        delete datos;
}

int SensoresDataModel::rowCount(const QModelIndex & /*parent*/) const
{
    return datos ? datos->size() : 0;
}

int SensoresDataModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 4;
}

QVariant SensoresDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch(section) {
        case 0:
            return QStringLiteral("Sensor Id");
        case 1:
            return QStringLiteral("Id");
        case 2:
            return QStringLiteral("Fecha/Hora");
        case 3:
            return QStringLiteral("Medición");
        }
    } else {
        return QStringLiteral("%1").arg(section);
    }

    return QVariant();
}

QVariant SensoresDataModel::data(const QModelIndex &index, int role) const
{

    if (role == Qt::DisplayRole && datos)
    {
        int i = index.row();
        DatoSensor * dato = datos->at(i);
        switch(index.column()) {
            case 0:
                return dato->getSensorId();
            case 1:
                return dato->getId();
            case 2:
                return dato->getTimestamp();
            case 3:
                return dato->getValor();
        }

        // Si no entro al switch entocnes retornar un valor standard "nro fila y nro col"
        return QString("Fila%1, Col%2")
                .arg(index.row() + 1)
                .arg(index.column() +1);
    }

    return QVariant();
}
