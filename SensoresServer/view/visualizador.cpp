#include "visualizador.h"
#include "ui_visualizador.h"
#include "sensoresdatamodel.h"
#include <QDebug>
#include <QThread>

Visualizador::Visualizador(ISensoresService *service, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Visualizador)
{
    ui->setupUi(this);

    this->service = service;
    this->sensores = new SensoresListModel(service->getAllSensors(), this);
    this->datos = new SensoresDataModel(nullptr, this);

    ui->listView->setModel(this->sensores);
    ui->tableView->setModel(this->datos);
    ui->tableView->setSelectionMode(QAbstractItemView::SingleSelection);

    setWindowTitle("Visor de datos");
}

Visualizador::~Visualizador()
{
    delete ui;
}

void Visualizador::on_listView_clicked(const QModelIndex &index)
{
    // Poner el cursor en modo Busy
    QGuiApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

    int i = index.row();

    Sensor * sensor = this->sensores->getListaSensores()->at(i);
    qDebug() << "Pos:" << i << ", Id:" << sensor->getId() << "Name:" << sensor->getNombre();
    QList<DatoSensor *> *datos = service->getAllSensorData(sensor);

    SensoresDataModel * oldModel = this->datos;

    this->datos = new SensoresDataModel(datos, this);
    ui->tableView->setModel(this->datos);

    if(oldModel)
        delete oldModel;

    // Para simular tiempo de acceso a db (y ver cursor Busy) descomanetar la siguiente linea
    // QThread::sleep(1);

    // Restaurar cursor por default
    QGuiApplication::restoreOverrideCursor();
}
