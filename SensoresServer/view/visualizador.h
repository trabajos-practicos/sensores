#ifndef VISUALIZADOR_H
#define VISUALIZADOR_H

#include <QDialog>
#include "view/sensoreslistmodel.h"
#include "view/sensoresdatamodel.h"
#include "service/isensoresservice.h"

namespace Ui {
class Visualizador;
}

class Visualizador : public QDialog
{
    Q_OBJECT

public:
    explicit Visualizador(ISensoresService *service, QWidget *parent = nullptr);
    ~Visualizador();

private slots:
    void on_listView_clicked(const QModelIndex &index);

private:
    Ui::Visualizador *ui;
    SensoresListModel * sensores;
    SensoresDataModel * datos;

    ISensoresService * service;
};

#endif // VISUALIZADOR_H
