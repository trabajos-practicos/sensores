#include "dialog.h"
#include "ui_dialog.h"
#include "sensoreslistmodel.h"
#include "net/networkDefinitions.h"

Dialog::Dialog(ISensoresService * service, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    this->visualizador = nullptr;
    this->server = new UdpServer(SERVER_PORT, this, this);
    this->service = service;

    this->ui->setupUi(this);
    this->setWindowTitle("Servidor de datos de sensores");
    this->updateEnabledItems();
}

Dialog::~Dialog()
{
    delete ui;
    delete server;

    if(visualizador)
        delete visualizador;
}

void Dialog::receive(QString msg)
{
    MedicionDeSensor * data;

    data = parser.parseMessage(msg);
    service->insertMedicion(data);

    log(msg);
}

void Dialog::log(QString msg)
{
    ui->listWidgetLog->addItem(msg);
    ui->listWidgetLog->setCurrentRow(ui->listWidgetLog->count()-1);
}

void Dialog::on_pushButtonVisualizador_clicked()
{
    if(!visualizador)
    {
        visualizador = new Visualizador(this->service, this);
    }
    visualizador->show();
    visualizador->raise();
    visualizador->activateWindow();
}

void Dialog::on_pushButtonIniciar_clicked()
{
    server->startServer();
    updateEnabledItems();
    log("Servidor iniciado");
}

void Dialog::on_pushButtonDetener_clicked()
{
    server->stopServer();
    updateEnabledItems();
    log("Servidor detenido");
}

void Dialog::updateEnabledItems()
{
    bool isStarted = server->isStarted();

    ui->pushButtonIniciar->setEnabled(!isStarted);
    ui->pushButtonDetener->setEnabled(isStarted);
}
