#ifndef SENSORESLISTMODEL_H
#define SENSORESLISTMODEL_H

#include <QAbstractListModel>
#include "model/sensor.h"
#include <QIcon>
#include <QPixmap>

// De acuerdo a la documentacion en:
//     https://doc.qt.io/qt-5/model-view-programming.html#model-classes
//     (se puede ver un ejemplo bajo el titulo "A read-only example model")
// es necesario implementar al menos estas 3 funciones:
//     1. rowCount()  : retorna la cantidad de filas que tiene el modelo
//     2. data()      : retorna que mostrar en una fila dada
//     3. headerData(): retorna titulos
class SensoresListModel : public QAbstractListModel
{
public:
    SensoresListModel(QList<Sensor*> * sensores, QObject *parent = nullptr);
    virtual ~SensoresListModel() override;

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    QList<Sensor*> * getListaSensores() { return sensores; }

private:
    QList<Sensor*> * sensores;
    QIcon iconTemp;
    QIcon iconMov;
    QIcon iconRuido;
};

#endif // SENSORESLISTMODEL_H
