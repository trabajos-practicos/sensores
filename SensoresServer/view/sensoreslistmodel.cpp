#include "sensoreslistmodel.h"
#include <QColor>
#include <QDebug>


SensoresListModel::SensoresListModel(QList<Sensor *> *sensores, QObject *parent /* = nullptr */)
    : QAbstractListModel(parent),
      iconTemp(":/icons/temp64x64.png"),
      iconMov(":/icons/mov64x64.png"),
      iconRuido(":/icons/ruido64x64.png")
{
    this->sensores = sensores;
//    qSort(this->sensores->begin(), this->sensores->end(), [] (const Sensor * s1, const Sensor * s2) {
//        return s1->getId() >= s2->getId();
//    });
//    std::sort(this->sensores->begin(), this->sensores->end());
}

SensoresListModel::~SensoresListModel()
{

}

int SensoresListModel::rowCount(const QModelIndex &/*parent*/) const
{
    return sensores->count();
}

QVariant SensoresListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (role == Qt::DecorationRole)
        return this->iconTemp;

    if (role == Qt::DisplayRole) {
        Sensor * s = sensores->at(index.row());
        return s->getNombre();
    } else {
        return QVariant();
    }
}

QVariant SensoresListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (role == Qt::DecorationRole)
        return QColor(255,0,0);

    if (orientation == Qt::Horizontal)
        return QStringLiteral("Column %1").arg(section);
    else
        return QStringLiteral("Row %1").arg(section);
}
