#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "visualizador.h"
#include "net/messagereceiver.h"
#include "net/messageparser.h"
#include "net/udpserver.h"
#include "service/isensoresservice.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog, MessageReceiver
{
    Q_OBJECT

public:
    explicit Dialog(ISensoresService * service, QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_pushButtonVisualizador_clicked();
    void on_pushButtonIniciar_clicked();
    void on_pushButtonDetener_clicked();

private:
    void receive(QString msg);
    void log(QString msg);
    void updateEnabledItems();

private:
    Ui::Dialog *ui;
    Visualizador * visualizador;
    UdpServer * server;
    MessageParser parser;
    ISensoresService * service;
};

#endif // DIALOG_H
