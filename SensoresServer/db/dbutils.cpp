#include "dbutils.h"
#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>

DbUtils::DbUtils(QObject *parent) : QObject(parent)
{
    databaseConnect();

    QStringList list = QSqlDatabase::database().tables(QSql::Tables);
    if(list.count() == 0) // Database ya inicializado
    {
        databaseInit();
    }
}
DbUtils::~DbUtils()
{
    QSqlDatabase::database().close();
}

void DbUtils::databaseConnect()
{
    const QString DRIVER("QSQLITE");

    if(QSqlDatabase::isDriverAvailable(DRIVER))
    {
        QSqlDatabase db = QSqlDatabase::addDatabase(DRIVER);
        db.setDatabaseName("db.sqlite");
        //db.setDatabaseName(":memory:");
        if(!db.open())
            qWarning() << __FUNCTION__ << " ERROR: " << db.lastError().text();
    }
    else
    {
        qWarning() << __FUNCTION__ << " ERROR: no driver " << DRIVER << " available";
    }
}

void DbUtils::databaseInit()
{
    QSqlQuery query;

    if( !query.exec("CREATE TABLE sensor (id INTEGER PRIMARY KEY, nombre VARCHAR(50), tipo VARCHAR(1))"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }

    // https://www.sqlite.org/datatype3.html:
    //   fecha como entero: INTEGER as Unix Time, the number of seconds since 1970-01-01 00:00:00 UTC.
    if( !query.exec("CREATE TABLE datos("
                    "id INTEGER PRIMARY KEY, "
                    "sensorid INTEGER, "
                    "fecha TEXT, "
                    "datoInt INTEGER, "
                    "datoFloat REAL, "
                    "datoBool INTEGER, "
                    "FOREIGN KEY(sensorid) REFERENCES sensor(id))"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }
}
