#ifndef DBUTILS_H
#define DBUTILS_H

#include <QObject>
#include <QSqlDatabase>

class DbUtils : public QObject
{
    Q_OBJECT
public:
    explicit DbUtils(QObject *parent = nullptr);
    virtual ~DbUtils();

signals:

public slots:

private:
    void databaseConnect();
    void databaseInit();
};

#endif // DBUTILS_H
