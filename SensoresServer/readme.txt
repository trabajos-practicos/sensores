model:
    Modelado de los datos con getters y setters

view:
    Logica de pantallas/vista.

service:
    Capa de reglas de negocio.
    Gestiona el modelo, el dao, y sus interacciones.
    Los frameworks de persistenacia (ie hibernate) facilitan mucho estas interacciones (nosotros las hacemos "a mano").

dao:
    Capa de persistencia.
    Manejo de bases de datos.
    Implementacion en SQLite.

controller:
    Capa que recibe la interaccion con el usuario (generalmente web).
    Tipicamente implementa una funion por URL mapping que retorna objetos JSON (REST API).
    Nosotros no usamos esta capa.

net:
    Gestion de UDP server sockets.
    Recepcion de los datos por UDP.
    Desgloce de mensajes.
