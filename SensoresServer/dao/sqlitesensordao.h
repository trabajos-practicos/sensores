#ifndef SQLITEENSORDAO_H
#define SQLITEENSORDAO_H

#include <QObject>
#include <QList>
#include <QSqlQuery>
#include "isensordao.h"

class SqliteSensorDao : public ISensorDao, QObject
{
public:
    explicit SqliteSensorDao(QObject *parent = nullptr);

    // Obtiene todos los sensores (temp, ruido, movimiento)
    QList<Sensor*> * getAll() override;

    Sensor* get(int id) override;
    bool insert(Sensor * sensor) override;
    bool remove(Sensor * sensor) override;
    bool update(Sensor * sensor) override;
};

#endif // SQLITEENSORDAO_H
