#include "sqlitedatosensordao.h"

#include <QSqlError>
#include <QDebug>
#include <QDateTime>

SqliteDatoSensorDao::SqliteDatoSensorDao(QObject *parent) : QObject(parent)
{
    queryInsertData.prepare("INSERT INTO datos(sensorid, fecha, datoInt, datoFloat, datoBool) "
                            "VALUES (:id, :fecha, :datoInt, :datoFloat, :datoBool)");
}

QList<DatoSensor*> * SqliteDatoSensorDao::getAll(Sensor * sensor) {
    QSqlQuery query;
    QList<DatoSensor*> * datos = new QList<DatoSensor*>();

    if( !query.exec("SELECT id, sensorid, fecha, datoInt, datoFloat, datoBool "
                    "FROM datos "
                    "WHERE sensorid=" + QString::number(sensor->getId())))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }

    QString campo;

    if(sensor->getTipo() == "T")
        campo = "datoFloat";
    else if(sensor->getTipo() == "M")
        campo = "datoInt";
    else
        campo = "datoBool";

    qDebug() << "Datos DB sensor Total: " << query.size();
    while(query.next())
    {
        DatoSensor * dato = new DatoSensor(query.value("sensorid").toInt(), query.value("id").toInt(), query.value("fecha").toDateTime(), query.value(campo).toString());
        datos->append(dato);
    }
    qDebug() << "Datos lista Total: " << datos->size();

    return datos;
}

bool SqliteDatoSensorDao::insert(Sensor * sensor, bool data) {
    QDateTime timestamp = QDateTime::currentDateTime();

    queryInsertData.bindValue(":id", sensor->getId());
    queryInsertData.bindValue(":fecha",  timestamp.toString("yyyy-MM-dd hh:mm:ss"));
    queryInsertData.bindValue(":datoInt",   QVariant(QVariant::Int));
    queryInsertData.bindValue(":datoFloat", QVariant(QVariant::Double));
    queryInsertData.bindValue(":datoBool",  data);

    if(!queryInsertData.exec())
    {
        qWarning() << __FUNCTION__ << " ERROR: " << queryInsertData.lastError().text();
        return false;
    }

    return true;
}

bool SqliteDatoSensorDao::insert(Sensor * sensor, float data) {
    QDateTime timestamp = QDateTime::currentDateTime();

    QSqlQuery q;

    queryInsertData.bindValue(":id", sensor->getId());
    queryInsertData.bindValue(":fecha",  timestamp.toString("yyyy-MM-dd hh:mm:ss"));
    queryInsertData.bindValue(":datoInt",   QVariant(QVariant::Int));
    queryInsertData.bindValue(":datoFloat", data);
    queryInsertData.bindValue(":datoBool",  QVariant(QVariant::Bool));

    if(!queryInsertData.exec())
    {
        qWarning() << "False: "  << queryInsertData.lastQuery();
        qWarning() << __FUNCTION__ << " ERROR: " << queryInsertData.lastError().text();
        return false;
    }

    qWarning() << "true: "<< queryInsertData.lastQuery();
    return true;
}

bool SqliteDatoSensorDao::insert(Sensor * sensor, int data) {
    QDateTime timestamp = QDateTime::currentDateTime();

    queryInsertData.bindValue(":id", sensor->getId());
    queryInsertData.bindValue(":fecha",  timestamp.toString("yyyy-MM-dd hh:mm:ss"));
    queryInsertData.bindValue(":datoInt",   data);
    queryInsertData.bindValue(":datoFloat", QVariant(QVariant::Double));
    queryInsertData.bindValue(":datoBool",  QVariant(QVariant::Bool));

    if(!queryInsertData.exec())
    {
        qWarning() << __FUNCTION__ << " ERROR: " << queryInsertData.lastError().text();
        return false;
    }

    return true;
}
