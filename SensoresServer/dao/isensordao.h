#ifndef ISENSORDAO_H
#define ISENSORDAO_H

#include <QList>
#include "model/sensor.h"
#include "model/datosensor.h"

// Interfaz para operaciones CRUD de sensores
class ISensorDao
{
public:
    ISensorDao();
    virtual ~ISensorDao();

    // Obtiene todos los sensores (temp, ruido, movimiento)
    virtual QList<Sensor*> * getAll() = 0;

    virtual Sensor* get(int id) = 0;
    virtual bool insert(Sensor * sensor) = 0;
    virtual bool remove(Sensor * sensor) = 0;
    virtual bool update(Sensor * sensor) = 0;
};

#endif // ISENSORDAO_H
