#ifndef IDATOSENSORDAO_H
#define IDATOSENSORDAO_H

#include <QList>
#include "model/sensor.h"
#include "model/datosensor.h"

// Interfaz para operaciones CRUD de daros de sensores
class IDatoSensorDao
{
public:
    IDatoSensorDao();
    virtual ~IDatoSensorDao();

    virtual QList<DatoSensor*> * getAll(Sensor * sensor) = 0;

    virtual bool insert(Sensor * sensor, bool data)  = 0;
    virtual bool insert(Sensor * sensor, float data) = 0;
    virtual bool insert(Sensor * sensor, int data)   = 0;
};

#endif // IDATOSENSORDAO_H
