#ifndef SQLITEDATOSENSORDAO_H
#define SQLITEDATOSENSORDAO_H

#include <QObject>
#include <QList>
#include <QSqlQuery>
#include "idatosensordao.h"

class SqliteDatoSensorDao : public IDatoSensorDao, QObject
{
public:
    explicit SqliteDatoSensorDao(QObject *parent = nullptr);

    // Obtiene todos los sensores (temp, ruido, movimiento)
    QList<DatoSensor*> * getAll(Sensor * sensor) override;

    bool insert(Sensor * sensor, bool data)  override;
    bool insert(Sensor * sensor, float data) override;
    bool insert(Sensor * sensor, int data)   override;

private:
    QSqlQuery queryInsertData;
};

#endif // SQLITEDATOSENSORDAO_H
