#include "sqlitesensordao.h"

#include <QSqlError>
#include <QDebug>
#include <QDateTime>

SqliteSensorDao::SqliteSensorDao(QObject *parent) : QObject(parent)
{

}

QList<Sensor*> * SqliteSensorDao::getAll() {
    QSqlQuery query;
    QList<Sensor*> * sensores = new QList<Sensor*>();

    if( !query.exec("SELECT id, nombre, tipo FROM sensor"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
    }

    while(query.next())
    {
        Sensor * sensor = new Sensor(query.value("id").toInt(), query.value("nombre").toString(), query.value("tipo").toString());
        sensores->append(sensor);
    }

    return sensores;
}

Sensor * SqliteSensorDao::get(int id) {
    QSqlQuery query;
    if( !query.exec("SELECT(id, nombre, tipo) FROM sensor WHERE id=" + QString::number(id)))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
        return nullptr;
    }
    return new Sensor(query.value("id").toInt(), query.value("nombre").toString(), query.value("tipo").toString());
}

bool SqliteSensorDao::insert(Sensor * sensor) {
    QSqlQuery query;

    if( !query.exec("INSERT INTO sensor(nombre, tipo) "
                    "VALUES ('" + sensor->getNombre() + "','" + sensor->getTipo() + "')"))
    {
        qWarning() << __FUNCTION__ << " ERROR: " << query.lastError().text();
        return false;
    }
    sensor->setId(query.lastInsertId().toInt());

    return true;
}

bool SqliteSensorDao::remove(Sensor * sensor) {
    Q_UNUSED(sensor);
    return false;
}

bool SqliteSensorDao::update(Sensor * sensor) {
    Q_UNUSED(sensor);
    return false;
}
