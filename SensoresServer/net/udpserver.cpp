#include "udpserver.h"
#include <QNetworkDatagram>

UdpServer::UdpServer(unsigned short port, MessageReceiver * receiver, QObject *parent)
    : QObject(parent)
{
    this->port = port;
    this->receiver = receiver;
    this->udpSocket = new QUdpSocket(this);
    this->started = false;

    connect(this->udpSocket, &QUdpSocket::readyRead, this, &UdpServer::readPendingDatagrams);
}

bool UdpServer::startServer()
{
    if(this->udpSocket->bind(QHostAddress::Any, this->port)) {
        started = true;
    }
    return started;
}

void UdpServer::stopServer()
{
    started = false;
    this->udpSocket->close();
}

bool UdpServer::isStarted()
{
    return started;
}

void UdpServer::readPendingDatagrams()
{
    while (udpSocket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = udpSocket->receiveDatagram();
        qDebug() << datagram.data();
        this->receiver->receive(datagram.data());
    }
}
