#ifndef UDPSENDER_H
#define UDPSENDER_H

#include <QObject>
#include <QUdpSocket>
#include "messagereceiver.h"

class UdpServer : public QObject
{
    Q_OBJECT

private:
    QUdpSocket  * udpSocket;
    MessageReceiver * receiver;
    unsigned short port;
    bool started;

public:
    UdpServer(unsigned short port, MessageReceiver * receiver, QObject *parent = nullptr);
    bool startServer();
    void stopServer();
    bool isStarted();

public slots:
    void readPendingDatagrams();
};

#endif // UDPSENDER_H
