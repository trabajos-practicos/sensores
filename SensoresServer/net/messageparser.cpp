#include "messageparser.h"
#include <QDebug>

MessageParser::MessageParser(QObject *parent) : QObject(parent)
{

}

MedicionDeSensor * MessageParser::parseMessage(QString message)
{
    // Formato esperado: "{X:id,valor}", donde X=T|M|R
    if(message.front() != '{' && message.back() != '}') {
        return nullptr;
    }

    message.remove('{').remove('}');
    QStringList m = message.split(":");

    if(m.size() != 3) {
        return nullptr;
    }

    return new MedicionDeSensor(m.at(1), m.at(0), m.at(2));
}
