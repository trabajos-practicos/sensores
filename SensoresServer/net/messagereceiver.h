#ifndef MSGRECEIVER_H
#define MSGRECEIVER_H

#include <QString>

class MessageReceiver
{
public:
    virtual void receive(QString msg) = 0;
    virtual ~MessageReceiver() { }
};

#endif // MSGRECEIVER_H
