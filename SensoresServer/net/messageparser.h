#ifndef MESSAGEPARSER_H
#define MESSAGEPARSER_H

#include <QObject>
#include <QSet>
#include "model/mediciondesensor.h"

class MessageParser : public QObject
{
    Q_OBJECT
public:
    explicit MessageParser(QObject *parent = nullptr);

signals:

public slots:

public:
    // Parsea los datos y los retorna en data.
    MedicionDeSensor * parseMessage(QString message);

private:
    // lista de sensores registrados
    QSet<QString> sensores;
};

#endif // MESSAGEPARSER_H
