#ifndef SENSOR_H
#define SENSOR_H

#include <QObject>
#include <QString>

class Sensor : public QObject
{
    Q_OBJECT
public:
    explicit Sensor(int id, QString nombre, QString tipo, QObject *parent = nullptr);

    void    setId(int id) { this->id = id; }

    int     getId() const { return id; }
    QString getNombre() const { return nombre; }
    QString getTipo() const { return tipo; }

signals:

public slots:

private:
    int id;
    QString nombre;
    QString tipo;
};

#endif // SENSOR_H
