#ifndef MEDICIONDESENSOR_H
#define MEDICIONDESENSOR_H

#include <QObject>
#include <QDateTime>

class MedicionDeSensor : public QObject
{
    Q_OBJECT
public:
    explicit MedicionDeSensor(QString nombre, QString tipo, QString valor, QObject *parent = nullptr);

signals:

public slots:

public:
    QString getTipo() const { return tipo; }
    QString getNombre() const { return nombre; }
    QString getValor() const { return valor; }
    QDateTime getTimestamp() const { return timestamp; }

private:
    QString tipo;
    QString nombre;
    QString valor;
    QDateTime timestamp;
};

#endif // MEDICIONDESENSOR_H
