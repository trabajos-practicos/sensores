#ifndef DATODESENSOR_H
#define DATODESENSOR_H

#include <QObject>
#include <QDateTime>

class DatoSensor : public QObject
{
    Q_OBJECT
public:
    explicit DatoSensor(int sensorId, int id, QDateTime timestamp, QString valor, QObject *parent = nullptr);

signals:

public slots:

public:
    int getSensorId() const { return sensorId; }
    int getId() const { return id; }
    QString getValor() const { return valor; }
    QDateTime getTimestamp() const { return timestamp; }

private:
    int sensorId;
    int id;
    QString valor;
    QDateTime timestamp;
};

#endif // DATODESENSOR_H
