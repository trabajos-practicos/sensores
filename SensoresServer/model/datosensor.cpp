#include "datosensor.h"

DatoSensor::DatoSensor(int sensorId, int id, QDateTime timestamp, QString valor, QObject *parent)
        : QObject(parent)
{
    this->sensorId  = sensorId;
    this->id        = id;
    this->timestamp = timestamp;
    this->valor     = valor;
}
