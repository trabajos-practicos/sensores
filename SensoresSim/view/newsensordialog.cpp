#include "newsensordialog.h"
#include "ui_newsensordialog.h"
#include <QMessageBox>
#include "model/sensortemp.h"

NewSensorDialog::NewSensorDialog(
    GestorSensores * gestorSensores,
    QWidget *parent)
        : QDialog(parent),
          ui(new Ui::NewSensorDialog)
{
    this->gestorSensores = gestorSensores;
    ui->setupUi(this);
}

NewSensorDialog::~NewSensorDialog()
{
    delete ui;
}

void NewSensorDialog::accept()
{
    QString id = ui->lineEditId->text().trimmed();

    if(id.length() < 1)
    {
        QMessageBox msgBox;
        msgBox.setText("Error de entrada de datos");
        msgBox.setInformativeText("El campo ID es obligatorio");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
        ui->lineEditId->setFocus();
    } else {
        if(ui->radioButtonTemp->isChecked())
        {
            sensor = gestorSensores->addSensorTemp(id, static_cast<uint16_t>(ui->spinBoxTiempo->value()));
        } else if(ui->radioButtonMov->isChecked()) {
            sensor = gestorSensores->addSensorMov(id, static_cast<uint16_t>(ui->spinBoxTiempo->value()));
        } else if(ui->radioButtonRuido->isChecked()) {
            sensor = gestorSensores->addSensorRuido(id, static_cast<uint16_t>(ui->spinBoxTiempo->value()));
        }

        QDialog::accept();
    }
}
