#include "dialog.h"
#include "ui_dialog.h"

#include <QMessageBox>

#include "newsensordialog.h"
#include "model/sensortemp.h"
#include "model/sensormov.h"
#include "model/sensorruido.h"
//#include "utils.h"

QTreeWidgetItem * Dialog::addRoot(QIcon icon, QString titulo, QString descripcion)
{
    QTreeWidgetItem * item = new QTreeWidgetItem(ui->treeWidget);
    item->setText(0, titulo);
    item->setIcon(0, icon);
    item->setText(1, descripcion);
    ui->treeWidget->addTopLevelItem(item);

    return item;
}

QTreeWidgetItem * Dialog::addChild(QTreeWidgetItem * parent, Sensor * sensor)
{
    QTreeWidgetItem * item = new QTreeWidgetItem(parent);
    item->setText(0, sensor->getId());
    item->setText(1, sensor->toString());
    item->setData(0, Qt::UserRole, QVariant::fromValue<Sensor*>(sensor));
    parent->addChild(item);

    return item;
}

Dialog::Dialog(
    GestorSensores * gestorSensores,
    QWidget *parent)
        : QDialog(parent),
          ui(new Ui::Dialog)
{
    this->gestorSensores = gestorSensores;

    ui->setupUi(this);
    ui->treeWidget->setColumnCount(2);
    ui->treeWidget->setHeaderLabels(QStringList({"Sensor", "Descripcion"}));
    ui->treeWidget->sortItems(0, Qt::AscendingOrder);

    this->treeRootItemMov   = addRoot(QIcon(":/icons/mov64x64.png"), "Movimiento", "Sensores de Movimiento");
    this->treeRootItemRuido = addRoot(QIcon(":/icons/ruido64x64.png"), "Ruido", "Sensores de Ruido");
    this->treeRootItemTemp  = addRoot(QIcon(":/icons/temp64x64.png"), "Temperatura", "Sensores de Temperatura");

    ui->pushButtonMinus->setDisabled(true);
    ui->pushButtonStop->setDisabled(true);

    setWindowTitle("Simulador de Sensores");
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButtonPlus_clicked()
{
    NewSensorDialog sensoresDlg(gestorSensores);
    int result;

    sensoresDlg.setModal(true);
    result = sensoresDlg.exec();

    QTreeWidgetItem * parent = nullptr;
    QTreeWidgetItem * child = nullptr;
    if(result == DialogCode::Accepted)
    {
        Sensor * sensor = sensoresDlg.getSensor();
        parent = getTreeRootItemFromSensor(sensor);
        child = addChild(parent, sensor);
    }

    if(parent)
        parent->setExpanded(true);
    if(child)
        ui->treeWidget->setCurrentItem(child);

    ui->treeWidget->sortItems(0, Qt::AscendingOrder);

    updateEnabledItems();
}

QTreeWidgetItem * Dialog::getTreeRootItemFromSensor(Sensor * sensor)
{
    SensorTemp  * temp  = qobject_cast<SensorTemp*>(sensor);
    SensorMov   * mov   = qobject_cast<SensorMov*>(sensor);
    if(temp) {
        return this->treeRootItemTemp;
    } else if(mov) {
        return this->treeRootItemMov;
    } else { // ruido
        return this->treeRootItemRuido;
    }
}

void Dialog::on_pushButtonMinus_clicked()
{
    // Obtengo el elemento seleccionado y verifico que no tenga padre (que sea un sensor)
    QTreeWidgetItem * item = ui->treeWidget->currentItem();
    if(item->parent() == nullptr)
        return;

    // Obtengo el sensor asociado al item seleccionado en pantalla
    Sensor * sensor = item->data(0, Qt::UserRole).value<Sensor *>();
    if(sensor == nullptr)
        return;

    // Elimino el sensor de la vista
    QTreeWidgetItem * parent = item->parent();
    parent->removeChild(item);
    delete item;

    // Elimino el sensor
    gestorSensores->remove(sensor);

    // Actualizo el estado enable/disable de la pantalla
    updateEnabledItems();
}

void Dialog::on_pushButtonStart_clicked()
{
    gestorSensores->start();
    updateEnabledItems();
}

void Dialog::on_pushButtonStop_clicked()
{
    gestorSensores->stop();
    updateEnabledItems();
}

void Dialog::on_treeWidget_itemSelectionChanged()
{
    updateEnabledItems();
}

void Dialog::updateEnabledItems()
{
    ui->pushButtonStart->setEnabled(!gestorSensores->isStarted());
    ui->pushButtonStop->setEnabled(gestorSensores->isStarted());
    ui->pushButtonMinus->setEnabled(ui->treeWidget->currentItem() != nullptr && ui->treeWidget->currentItem()->parent() != nullptr && !gestorSensores->isStarted());
    ui->pushButtonPlus->setEnabled(!gestorSensores->isStarted());
}
