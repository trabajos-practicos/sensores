#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTreeWidget>
#include "model/sensor.h"
#include "service/gestorsensores.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(
            GestorSensores * gestorSensores,
            QWidget *parent = nullptr);

    ~Dialog();

private slots:
    void on_pushButtonPlus_clicked();
    void on_pushButtonMinus_clicked();
    void on_pushButtonStart_clicked();
    void on_pushButtonStop_clicked();

    void on_treeWidget_itemSelectionChanged();

private:
    Ui::Dialog *ui;
    GestorSensores * gestorSensores;
    QTreeWidgetItem * treeRootItemTemp;
    QTreeWidgetItem * treeRootItemRuido;
    QTreeWidgetItem * treeRootItemMov;

    void updateEnabledItems();
    QTreeWidgetItem * addRoot(QIcon icon, QString titulo, QString descripcion);
    QTreeWidgetItem * addChild(QTreeWidgetItem * parent, Sensor * sensor);
    QTreeWidgetItem * getTreeRootItemFromSensor(Sensor * sensor);
};

#endif // DIALOG_H
