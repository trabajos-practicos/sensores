#ifndef NEWSENSORDIALOG_H
#define NEWSENSORDIALOG_H

#include <QDialog>
#include "utils.h"
#include "model/sensor.h"
#include "service/gestorsensores.h"

namespace Ui {
class NewSensorDialog;
}

class NewSensorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewSensorDialog(
            GestorSensores * gestorSensores,
            QWidget *parent = nullptr);

    ~NewSensorDialog() override;

    Sensor * getSensor() { return sensor; }

public slots:
    void accept() override;

private:
    Ui::NewSensorDialog *ui;
    Sensor * sensor;
    GestorSensores * gestorSensores;
};

#endif // NEWSENSORDIALOG_H
