#ifndef UTILS_H
#define UTILS_H

/**
 * @brief getRand Funcion para obtener un numero aleatorio entre dos numeros dados.
 * @param min Valor minimo a obtener
 * @param max Valor maximo a obtener.
 * @return Numero aleatorio entre min y max
 */
int getRand(int min, int max);

#endif // UTILS_H
