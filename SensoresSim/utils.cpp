#include <QRandomGenerator>

static QRandomGenerator r;

int getRand(int min, int max)
{
    return r.bounded(min, max);
}
