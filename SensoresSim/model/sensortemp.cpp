#include "sensortemp.h"
#include <QDebug>
#include "utils.h"

SensorTemp::SensorTemp(
        QString id,
        uint16_t segundosMedicion,
        QString serverHostname,
        uint16_t serverPortNumber,
        QObject *parent)
        : Sensor(id, segundosMedicion, serverHostname, serverPortNumber, parent)
{
    // Elijo una zona inicial al azar donde podria estar el sensor: 1:calido, 2:normal o 3:frio
    int zona = getRand(1, 3);
    temperatura = zona == 1 ? getRand(30, 55) :
                  zona == 2 ? getRand(10, 35) :
                              getRand(-10, 15);
}

QString SensorTemp::tomarMedicion()
{
    int cambio = 0;

    cambio = getRand(-3, 3); // porcentaje del cambio con respecto a muestra anterior
    temperatura += temperatura * cambio / 100.0F;

    if(temperatura <= -10) temperatura = -9.9F;
    if(temperatura >= 55) temperatura = 54.9F;

    return "{T:" + id + ":" + QString::number(static_cast<double>(temperatura)) + "}";
}

QString SensorTemp::toString()
{
    return "[T-" + Sensor::toString() + "]";
}
