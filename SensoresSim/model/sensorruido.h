#ifndef SENSORRUIDO_H
#define SENSORRUIDO_H

#include <QObject>
#include "sensor.h"

class SensorRuido : public Sensor
{
    Q_OBJECT
public:
    SensorRuido(QString id,
                uint16_t segundosMedicion,
                QString serverHostname,
                uint16_t serverPortNumber,
                QObject *parent = nullptr);

    QString tomarMedicion() override;
    QString toString() override;

private:
    int ruido;
    int probabilidadDeRuido;
};

#endif // SENSORRUIDO_H
