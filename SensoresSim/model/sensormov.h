#ifndef SENSORMOV_H
#define SENSORMOV_H

#include <QObject>
#include "sensor.h"

class SensorMov : public Sensor
{
    Q_OBJECT
public:
    SensorMov(QString id,
              uint16_t segundosMedicion,
              QString serverHostname,
              uint16_t serverPortNumber,
              QObject *parent = nullptr);

    QString tomarMedicion() override;
    QString toString() override;

private:
    bool movimiento;
    int probabilidadDeMovimiento;
};

#endif // SENSORMOV_H
