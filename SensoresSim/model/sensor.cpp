#include "sensor.h"
#include <QDebug>
#include <QString>
#include <QTime>

Sensor::Sensor(
    QString id,
    uint16_t segundosMedicion,
    QString serverHostname,
    uint16_t serverPortNumber,
    QObject *parent)
    : QObject(parent),
      id(id),
      serverHostname(serverHostname),
      serverPortNumber(serverPortNumber),
      segundosMedicion(segundosMedicion),
      timer(this)
{
    address.setAddress(serverHostname);

    connect(&timer, SIGNAL(timeout()), this, SLOT(enviarDatos()));
}

/**
 * @brief Sensor::enviarDatos
 * bla
 */
void Sensor::enviarDatos()
{
    QString mensaje = tomarMedicion();
    qDebug() << "[" + QTime::currentTime().toString("hh:mm:ss") + "]" +
                " [" + address.toString() + ":" + QString::number(serverPortNumber) + "]" +
                " <-- " + mensaje;
    socket.writeDatagram(mensaje.toUtf8(), address, serverPortNumber);
}

void Sensor::start()
{
    timer.start(segundosMedicion * 1000);
}

void Sensor::stop()
{
    timer.stop();
    disconnect(&timer, &QTimer::timeout, this, &Sensor::enviarDatos);
}

QString Sensor::toString()
{
    return id + " (" + QString::number(segundosMedicion) + ") " + serverHostname + ":" + QString::number(serverPortNumber);
}
