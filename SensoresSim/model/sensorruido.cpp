#include "sensorruido.h"
#include <QDebug>
#include "utils.h"

SensorRuido::SensorRuido(
        QString id,
        uint16_t segundosMedicion,
        QString serverHostname,
        uint16_t serverPortNumber,
        QObject *parent)
        : Sensor(id, segundosMedicion, serverHostname, serverPortNumber, parent)
{
    // Determino una zona al azar donde podria estar el sensor: 1:mucho ruido, 2:ruido normal o 3:poco ruido
    int zona = getRand(1, 3);
    ruido = zona == 1 ? getRand(110, 140) :
            zona == 2 ? getRand( 80, 110) :
                        getRand( 10, 70);
}

QString SensorRuido::tomarMedicion()
{
    // Calcular ruido
    int cambio = 0;

    cambio = getRand(-3, 3); // porcentaje del cambio con respecto a muestra anterior
    ruido += ruido * cambio / 100.0F;

    if(ruido <= 10) ruido = 10;
    if(ruido >= 140) ruido = 140;

    return "{R:" + id + ":" + QString::number(static_cast<int>(ruido)) + "}";
}

QString SensorRuido::toString()
{
    return "[R-" + Sensor::toString() + "]";
}
