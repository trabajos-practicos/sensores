#ifndef SENSORTEMP_H
#define SENSORTEMP_H

#include <QObject>
#include "sensor.h"

class SensorTemp : public Sensor
{
    Q_OBJECT
public:
    SensorTemp( QString id,
                uint16_t segundosMedicion,
                QString serverHostname,
                uint16_t serverPortNumber,
                QObject *parent = nullptr);

    QString tomarMedicion() override;
    QString toString() override;

private:
    float temperatura;
};

#endif // SENSORTEMP_H
