#ifndef SENSOR_H
#define SENSOR_H

#include <QObject>
#include <QTimer>
#include <QUdpSocket>

class Sensor : public QObject
{
    Q_OBJECT
public:
    explicit Sensor(
        QString id,
        uint16_t segundosMedicion,
        QString serverHostname,
        uint16_t serverPortNumber,
        QObject *parent = nullptr);

    virtual void start();
    virtual void stop();
    virtual QString toString();

    QString getId() const { return id; }

signals:

public slots:
    void enviarDatos();

protected:
    virtual QString tomarMedicion() = 0;

    QString id;
    QString serverHostname;
    uint16_t serverPortNumber;
    uint16_t segundosMedicion;

    QTimer timer;
    QUdpSocket socket;
    QHostAddress address;
};

#endif // SENSOR_H
