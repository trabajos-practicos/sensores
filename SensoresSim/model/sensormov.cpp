#include "sensormov.h"
#include <QDebug>
#include "utils.h"

SensorMov::SensorMov(
        QString id,
        uint16_t segundosMedicion,
        QString serverHostname,
        uint16_t serverPortNumber,
        QObject *parent)
        : Sensor(id, segundosMedicion, serverHostname, serverPortNumber, parent)
{
    // Determino una zona al azar donde podria estar el sensor: 1:mucho trafico, 2:trafico normal o 3:poco trafico
    int zona = getRand(1, 3);
    this->probabilidadDeMovimiento =
            zona == 1 ? 75 :
            zona == 2 ? 50 : 25;
}

QString SensorMov::tomarMedicion()
{
    // Calcular ruido
    this->movimiento = (getRand(0, 100) >= probabilidadDeMovimiento);

    return "{M:" + id + ":" + QString::number(static_cast<bool>(this->movimiento)) + "}";
}

QString SensorMov::toString()
{
    return "[M-" + Sensor::toString() + "]";
}
