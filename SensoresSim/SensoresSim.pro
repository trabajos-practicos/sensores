#-------------------------------------------------
#
# Project created by QtCreator 2019-07-09T11:06:28
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SensoresSim
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

#INCLUDEPATH += ./model
#INCLUDEPATH += ./service
#INCLUDEPATH += ./view
#INCLUDEPATH += ./net

SOURCES += \
        model/sensormov.cpp \
        model/sensorruido.cpp \
        model/sensortemp.cpp \
        model/sensor.cpp \
        view/dialog.cpp \
        view/newsensordialog.cpp \
        service/gestorsensores.cpp \
        utils.cpp \
        main.cpp

HEADERS += \
        model/sensormov.h \
        model/sensorruido.h \
        model/sensor.h \
        model/sensortemp.h \
        view/dialog.h \
        view/newsensordialog.h \
        service/gestorsensores.h \
        net/networkDefinitions.h \
        utils.h

FORMS += \
        view/dialog.ui \
        view/newsensordialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

VERSION = 1.0.1.0
win32:RC_ICONS = icons/appsensoressim.ico
QMAKE_TARGET_COMPANY = Company Martin
QMAKE_TARGET_DESCRIPTION = Aplicacion de simulación de sensores
QMAKE_TARGET_COPYRIGHT = Este producto tiene Copyright de pirulo. Use at your own risk.
QMAKE_TARGET_PRODUCT = Sensores Server
