#ifndef GESTORSENSORES_H
#define GESTORSENSORES_H

#include <QObject>
#include "model/sensor.h"

class GestorSensores : public QObject
{
    Q_OBJECT
public:
    explicit GestorSensores(QObject *parent = nullptr);
    virtual ~GestorSensores();

    Sensor * addSensorTemp(QString id, uint16_t segundosMedicion);
    Sensor * addSensorMov(QString id, uint16_t segundosMedicion);
    Sensor * addSensorRuido(QString id, uint16_t segundosMedicion);

    void add(Sensor *);
    void remove(Sensor *);
    void start();
    void stop();

    bool isStarted() { return started; }

signals:

public slots:

private:
    QList<Sensor *> sensores;
    bool started;
};

#endif // GESTORSENSORES_H
