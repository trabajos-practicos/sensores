#include "service/gestorsensores.h"
#include "net/networkDefinitions.h"
#include <model/sensortemp.h>
#include <model/sensorruido.h>
#include <model/sensormov.h>

GestorSensores::GestorSensores(QObject *parent) : QObject(parent), started(false)
{

}

GestorSensores::~GestorSensores()
{
    while( ! sensores.isEmpty() )
        delete sensores.takeLast();
}

void GestorSensores::add(Sensor * s)
{
    sensores.append(s);
}

void GestorSensores::remove(Sensor * sensor)
{
    sensores.removeOne(sensor);
    delete sensor;
}

Sensor * GestorSensores::addSensorTemp(QString id, uint16_t segundosMedicion)
{
    Sensor * s = new SensorTemp(id, segundosMedicion, SERVER_ADDRESS, SERVER_PORT);
    add(s);
    return s;
}

Sensor * GestorSensores::addSensorMov(QString id, uint16_t segundosMedicion)
{
    Sensor * s = new SensorMov(id, segundosMedicion, SERVER_ADDRESS, SERVER_PORT);
    add(s);
    return s;
}

Sensor * GestorSensores::addSensorRuido(QString id, uint16_t segundosMedicion)
{
    Sensor * s = new SensorRuido(id, segundosMedicion, SERVER_ADDRESS, SERVER_PORT);
    add(s);
    return s;
}

void GestorSensores::start()
{
    foreach (Sensor * sensor, sensores) {
        sensor->start();
    }
    started = true;
}

void GestorSensores::stop()
{
    foreach (Sensor * sensor, sensores) {
        sensor->stop();
    }
    started = false;
}
