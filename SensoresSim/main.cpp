#include "view/dialog.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    GestorSensores gestorSensores;
    Dialog w(&gestorSensores);
    w.show();

    return a.exec();
}
