TEMPLATE = subdirs

SUBDIRS += \
    sensoresSim \
    sensoresServer

QMAKE_CXXFLAGS += -std=gnu++14
